// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const cors = require("cors");
// CORS (Cross-Origin Resource Sharing) is a security feature implemented by web browsers that blocks web pages from making requests to a different domain than the one that served the web page. This is done to prevent malicious websites from stealing sensitive information from other websites\
//In Node.js, CORS can be implemented using a middleware package such as cors. It allows you to configure the server to allow or disallow specific origins and HTTP methods. This can be useful if you are building a web application that makes API calls to a server running on a different domain.

// Routes

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();
const port = 3010;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
/*
// Database Connection
// Connect to MongoDB Atlas
*/
mongoose.connect(`mongodb+srv://vrosaldonovulutions:${process.env.PASSWORD}@cluster0.qikdkns.mongodb.net/course-booking-api?retryWrites=true&w=majority`,


	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Setup notifcation for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB!"));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// Server listening
app.listen(port, () => console.log(`Server running at port ${port}!`));

console.log("Please dont stop this server.");

