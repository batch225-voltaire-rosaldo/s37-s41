const Course = require("../models/course");

// Activity #4 Jan. 23, 2023 =====================================

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

// ====================================================
module.exports.addCourse = (data) => {
    // Uses the information from the request body to provide all the necessary information.
    if(data.isAdmin) {
    let new_course = new Course({
        name : data.course.name,
        description : data.course.description,
        price : data.course.price
    });

    return new_course.save().then((new_course, error) => {
        //
        if(error) {
            return false;
            //course creation successful
        }
            return {
                message: "New course successfully created!"
            }
    });
}
    let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})

};