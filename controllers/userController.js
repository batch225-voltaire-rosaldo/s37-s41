//const { trusted } = require("mongoose");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.loginUser = (reqBody) => {

    return User.findOne({email : reqBody.email}).then (
        result => {
            if(result == null){
                return {
                    message: "Not found in our database"
                }
            } else {
                const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

                if(isPasswordCorrect) {
                    return{access : auth.createAccessToken(result)}
                } else {
                    return {
                        message : "password was incorrect"
                    }
                } 
            }
        })
};




// Activity No. 2 (User Details) - Jan 19, 2023

/*
module.exports.getProfile = (data) => {
    
    return User.findById(data.userID).then(result => {
        
        // Changes the value of the user's password to an empty string
        // Not doing so will expose the user's password which will 
        // Unlike in the "register" method, we do not need to 
        result.password = "";

        // Returns the user information with the password as an 
        return result;
    })
};
*/

module.exports.checkUserDetails = (reqBody) => {
    return User.find({_id : reqBody.id}).then(result => {
        if (result.length > 0) {
            return result;
        } else if (result.length >1) {
            return {
                message: "Duplicate User"
            }
        } else {
            return {
                message: "User not found"
            }
        }
    })
};

// ==========================================

module.exports.checkEmailExists = (reqBody) => {
    
    return User.find({email : reqBody.email}).then(result => {
        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return {
                message: "User was found"
            }
        } else if (result.length >1) {
            return {
                message: "Duplicate User"
            }
        } else {
            return {
                message: "User not found"
            }
        }
    })
};

module.exports.registerUser = (reqBody) => {
    // Create a variable "new"
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return user;
        }
    });
};

// User Authentication
