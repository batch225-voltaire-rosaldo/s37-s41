const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

// Router for creating a course

router.post("/addCourse", auth.verify,  (req, res) => {  
    // 
    const data = {
        course : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    courseController.addCourse(data).then(result => res.send(result));
});

// Activity #4 Jan.23, 2023 ========================================
router.get("/getCourse", (req, res) => {
    courseController.getAllCourses().then(result => res.send(result));
});
// =================================================

module.exports = router;