const express = require("express");
const router = express.Router();

// Import
const userController = require("../controllers/userController");

// Check Email
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Route for Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result));
});

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(result => {
        res.send(result)
    })

});

// Activity No. 2 (User Details)- Jan 19, 2023

router.post("/details", (req, res) => {
    // Provides the user's ID for the getProfile controller method
    // userController.getProfile({userID : req.body.id}).then(resultFromController => res.send(resultFromController));
    
    userController.checkUserDetails(req.body).then(result => res.send(result));
});

//=============================

module.exports = router;